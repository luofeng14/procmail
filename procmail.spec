# hardened build if not overridden
%{!?_hardened_build:%global _hardened_build 1}

%if %{?_hardened_build}%{!?_hardened_build:0}
%global hardened_flags -pie -Wl,-z,relro,-z,now
%endif

Name:               procmail
Version:            3.24
Release:            2
Summary:            A program for filtering, sorting and storing email
License:            GPLv2+ or Artistic-1.0-Perl
URL:                https://github.com/BuGlessRB/%{name}
Source0:            https://github.com/BuGlessRB/%{name}/archive/v%{version}/%{name}-%{version}.tar.gz
Source1:            procmailrc
Patch0:             procmail-3.24-rhconfig.patch
Patch1:             procmail-3.15.1-man.patch
Patch2:             procmail-3.22-truncate.patch
Patch3:             procmail-3.24-ipv6.patch
Patch4:             procmail-3.24-support-clang-build.patch

BuildRequires:      make  gcc 

%description
%{summary},It can be used both on mail clients and mail servers. It can be
used to filter out spam, checking for viruses, to send automatic replies, etc.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

find examples -type f | xargs chmod 644

%build
make RPM_OPT_FLAGS="$(getconf LFS_CFLAGS)" autoconf.h
%make_build

%install
rm -rf ${RPM_BUILD_ROOT}
mkdir -p ${RPM_BUILD_ROOT}%{_bindir} ${RPM_BUILD_ROOT}%{_mandir}/man{1,5}

make \
    BASENAME=${RPM_BUILD_ROOT}%{_prefix} MANDIR=${RPM_BUILD_ROOT}%{_mandir} \
    install

cp -p %{SOURCE1} telsas_procmailrc

%files
%defattr(-,root,root)
%doc Artistic
%license COPYING
%{_bindir}/*

%files help
%defattr(-,root,root)
%doc FAQ FEATURES HISTORY README KNOWN_BUGS examples telsas_procmailrc
%{_mandir}/man1/*.1.gz
%{_mandir}/man5/*.5.gz

%changelog
* Mon Mar 25 2024 luofeng <luofeng13@huawei.com> - 3.24-2
- support clang build

* Mon Jul 24 2023 yanglu <yanglu72@h-partners.com> - 3.24-1
- DESC:Update procmail version to 3.24

* Tue Jul 5 2022 Chenyx <chenyixiong3@huawei.com> - 3.22-53
- License compliance rectification

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 3.22-52
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Thu Sep 10 2020 hanzhijun<hanzhijun1@huawei.com> - 3.22-51
- fix source url problem

* Mon Oct 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.22-50
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:modify the location of COPYING

* Mon Sep 9 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.22-49
- Package init

